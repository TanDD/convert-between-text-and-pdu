<?php

namespace Tandd\PduFactory;

const ALPHABET_SIZE_7  = 7;
const ALPHABET_SIZE_8  = 8;
const ALPHABET_SIZE_16 = 16;
const CHAR_16BIT       = [
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    'A' => 10,
    'B' => 11,
    'C' => 12,
    'D' => 13,
    'E' => 14,
    'F' => 15,
];
class Pdu
{
    private static $instance = null;

    private $hex = '0123456789ABCDEF';

    private $maxChars = 160;

    //Array with "The 7 bit defaultalphabet"
    private $sevenBitDefault = [
        '@',
        '£',
        '$',
        '¥',
        'è',
        'é',
        'ù',
        'ì',
        'ò',
        'Ç',
        '\n',
        'Ø',
        'ø',
        '\r',
        'Å',
        'å',
        'Δ',
        '_',
        'Φ',
        'Γ',
        'Λ',
        'Ω',
        'Π',
        'Ψ',
        'Σ',
        'Θ',
        'Ξ',
        '☝',
        'Æ',
        'æ',
        'ß',
        'É',
        ' ',
        '!',
        '"',
        '#',
        '¤',
        '%',
        '&',
        '\'',
        '(',
        ')',
        '*',
        '+',
        ',',
        '-',
        '.',
        '/',
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        ':',
        ';',
        '<',
        '=',
        '>',
        '?',
        '¡',
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z',
        'Ä',
        'Ö',
        'Ñ',
        'Ü',
        '§',
        '¿',
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z',
        'ä',
        'ö',
        'ñ',
        'ü',
        'à',
    ];

    private $sevenBitExtended = [
        '\f',
        0x0A,    // '\u000a',	// <FF>
        '^',
        0x14,    // '\u0014',	// CIRCUMFLEX ACCENT
        '{',
        0x28,    // '\u0028',	// LEFT CURLY BRACKET
        '}',
        0x29,    // '\u0029',	// RIGHT CURLY BRACKET
        '\\',
        0x2F,    // '\u002f',	// REVERSE SOLIDUS
        '[',
        0x3C,    // '\u003c',	// LEFT SQUARE BRACKET
        '~',
        0x3D,    // '\u003d',	// TILDE
        ']',
        0x3E,    // '\u003e',	// RIGHT SQUARE BRACKET
        '|',
        0x40,    // '\u0040',	// VERTICAL LINE \u7c
        '€',
        0x65    // '\u0065'	// EURO SIGN &#8364;
    ];

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Pdu();
        }

        return self::$instance;
    }

    public function pduToText($pduString)
    {
        $out             = [];
        $SMSC_lengthInfo = $this->hexToNum($this->subString($pduString, 0, 2));
        $SMSC_info       = $this->subString($pduString, 2, 2 + ($SMSC_lengthInfo * 2));
        $SMSC_Number     = $this->subString($SMSC_info, 2, 2 + ($SMSC_lengthInfo * 2));
        if ($SMSC_lengthInfo !== 0) {
            $SMSC_Number = $this->semiOctetToString($SMSC_Number);
            // if the length is odd remove the trailing  F
            if (strtoupper($this->substr($SMSC_Number, $this->stringLength($SMSC_Number) - 1, 1)) === 'F') {
                $SMSC_Number = $this->subString($SMSC_Number, 0, $this->stringLength($SMSC_Number) - 1);
            }
        }
        $start                 = ($SMSC_lengthInfo * 2) + 2;
        $firstOctet_SMSDeliver = $this->substr($pduString, $start, 2);
        $start                 = $start + 2;
        $userDataHeader        = 0;
        if (($this->hexToNum($firstOctet_SMSDeliver) & 0x40) === 0x40) {
            $userDataHeader = 1;
        }
        $hex_dump = [];
        if (($this->hexToNum($firstOctet_SMSDeliver) & 0x03) === 1 || ($this->hexToNum($firstOctet_SMSDeliver) & 0x03) === 3) // Transmit Message
        {
            $out['smsSubmit'] = 'send';
            if (($this->hexToNum($firstOctet_SMSDeliver) & 0x03) === 3) {
                $out[] = "Unknown Message";
                $out[] = "Treat as Deliver";
            }
            if (($this->hexToNum($firstOctet_SMSDeliver) & 0x20) === 0x20) {
                $rr = 'yes';
            } else {
                $rr = 'no';
            }
            $out['receiptRequested'] = $rr;
            $start                   = $start + 2;
            // length in decimals
            $sender_addressLength = $this->hexToNum($this->substr($pduString, $start, 2));
            if ($sender_addressLength % 2 !== 0) {
                $sender_addressLength += 1;
            }
            $start               = $start + 2;
            $senderTypeOfAddress = $this->substr($pduString, $start, 2);
            $start               = $start + 2;
            $sender_number       = $this->semiOctetToString($this->subString($pduString, $start, $start + $sender_addressLength));
            if (strtoupper($this->substr($sender_number, $this->stringLength($sender_number) - 1, 1)) === 'F') {
                $sender_number = $this->subString($sender_number, 0, $this->stringLength($sender_number) - 1);
            }
            $start          += $sender_addressLength;
            $tp_PID         = $this->substr($pduString, $start, 2);
            $start          += 2;
            $tp_DCS         = $this->substr($pduString, $start, 2);
            $tp_DCS_desc    = $this->tpDCSMeaning($tp_DCS);
            $start          += 2;
            $ValidityPeriod = '';
            switch (($this->hexToNum($firstOctet_SMSDeliver) & 0x18)) {
                case 0: // Not Present
                    $ValidityPeriod = 'Not Present';
                    break;
                case 0x10: // Relative
                    $ValidityPeriod = 'Rel '.$this->cValid($this->hexToNum($this->substr($pduString, $start, 2)));
                    $start          += 2;
                    break;
                case 0x08: // Enhanced
                    $ValidityPeriod = 'Enhanced - Not Decoded';
                    $start          += 14;
                    break;
                case 0x18: // Absolute
                    $ValidityPeriod = 'Absolute - Not Decoded';
                    $start          += 14;
                    break;
            }
            $messageLength  = $this->hexToNum($this->substr($pduString, $start, 2));
            $start          += 2;
            $bitSize        = $this->dcsBits($tp_DCS);
            $userData       = 'Undefined format';
            $skipCharacters = 0;
            if (($bitSize === ALPHABET_SIZE_7 || $bitSize === ALPHABET_SIZE_16) && $userDataHeader) {
                $ud_len         = $this->hexToNum($this->substr($pduString, $start, 2));
                $userDataHeader = '';
                for ($i = 0; $i <= $ud_len; $i++) {
                    $userDataHeader .= $this->substr($pduString, $start + $i * 2, 2).' ';
                }
                if ($bitSize === ALPHABET_SIZE_7) {
                    $skipCharacters = ((($ud_len + 1) * 8) + 6) / 7;
                } else {
                    $skipCharacters = ($ud_len + 1) / 2;
                }
            }
            if ($bitSize === ALPHABET_SIZE_7) {
                $userData = $this->getUserMessage($skipCharacters, $this->substr($pduString, $start, $this->stringLength($pduString) - $start), $messageLength);
            } else {
                if ($bitSize === ALPHABET_SIZE_8) {
                    $userData = $this->getUserMessage8($skipCharacters, $this->substr($pduString, $start, $this->stringLength($pduString) - $start), $messageLength);
                    for ($i = 0; $i < $this->stringLength($userData); $i++) {
                        if ($this->substr($userData, $i, 1) >= ' ') {
                            $hex_dump[] = [
                                'hex'     => $this->intToHex($this->charCodeAt($userData, $i)),
                                'pointer' => $this->substr($userData, $i, 1),
                            ];
                        } else {
                            $hex_dump[] = [
                                'hex'     => $this->intToHex($this->charCodeAt($userData, $i)),
                                'pointer' => '.',
                            ];
                        }
                    }
                } else {
                    if ($bitSize === ALPHABET_SIZE_16) {
                        $userData = $this->getUserMessage16($skipCharacters, $this->substr($pduString, $start, $this->stringLength($pduString) - $start));
                    }
                }
            }
            $userData = $this->substr($userData, 0, $messageLength);
            if ($bitSize === ALPHABET_SIZE_16) {
                $messageLength /= 2;
            }
            $out['smsc']          = $SMSC_Number;
            $out['recipient']     = $sender_number;
            $out['toa']           = $senderTypeOfAddress;
            $out['numberingPlan'] = $this->explainToa($senderTypeOfAddress);
            $out['validity']      = $ValidityPeriod;
            $out['tpPid']         = $tp_PID;
            $out['tpDcs']         = $tp_DCS;
            $out['tpDcsDesc']     = $tp_DCS_desc;
            if ($userDataHeader !== '') {
                $out['userDataHeader'] = $userDataHeader;
            }
            $out['encoding'] = mb_detect_encoding($userData);
            $out['length']   = $messageLength;
            $out['message']  = $userData;
            if (! empty($hex_dump)) {
                $out['hexadecimalDump'] = $hex_dump;
            }
        } else // Receive Message
        {
            if (($this->hexToNum($firstOctet_SMSDeliver) & 0x03) === 0) // Receive Message
            {
                $out['smsDeliver'] = 'receive';
                if (($this->hexToNum($firstOctet_SMSDeliver) & 0x20) === 0x20) {
                    $rr = 'yes';
                } else {
                    $rr = 'no';
                }
                $out['receiptRequested'] = $rr;
                // length in decimals
                $sender_addressLength = $this->hexToNum($this->substr($pduString, $start, 2));
                $start                = $start + 2;
                $senderTypeOfAddress  = $this->substr($pduString, $start, 2);
                $start                = $start + 2;
                if ($senderTypeOfAddress === 'D0') {
                    $_sl = $sender_addressLength;
                    if ($sender_addressLength % 2 !== 0) {
                        $sender_addressLength += 1;
                    }
                    $sender_number = $this->getUserMessage(0, $this->subString($pduString, $start, $start + $sender_addressLength), $this->parseInt($_sl / 2 * 8 / 7));
                } else {
                    if ($sender_addressLength % 2 !== 0) {
                        $sender_addressLength += 1;
                    }
                    $sender_number = $this->semiOctetToString($this->subString($pduString, $start, $start + $sender_addressLength));
                    if (strtoupper($this->substr($sender_number, $this->stringLength($sender_number) - 1, 1)) === 'F') {
                        $sender_number = $this->subString($sender_number, 0, $this->stringLength($sender_number) - 1);
                    }
                }
                $start       += $sender_addressLength;
                $tp_PID      = $this->substr($pduString, $start, 2);
                $start       += 2;
                $tp_DCS      = $this->substr($pduString, $start, 2);
                $tp_DCS_desc = $this->tpDCSMeaning($tp_DCS);
                $start       += 2;
                $timeStamp   = $this->semiOctetToString($this->substr($pduString, $start, 14));
                // get date
                $year           = $this->subString($timeStamp, 0, 2);
                $month          = $this->subString($timeStamp, 2, 4);
                $day            = $this->subString($timeStamp, 4, 6);
                $hours          = $this->subString($timeStamp, 6, 8);
                $minutes        = $this->subString($timeStamp, 8, 10);
                $seconds        = $this->subString($timeStamp, 10, 12);
                $timezone       = $this->subString($timeStamp, 12, 14);
                $timeStamp      = new \DateTime($year.'-'.$month.'-'.$day.' '.$hours.':'.$minutes.':'.$seconds.' GMT');
                $start          += 14;
                $messageLength  = $this->hexToNum($this->substr($pduString, $start, 2));
                $start          += 2;
                $bitSize        = $this->dcsBits($tp_DCS);
                $userData       = 'Undefined format';
                $skipCharacters = 0;
                if (($bitSize === ALPHABET_SIZE_7 || $bitSize === ALPHABET_SIZE_16) && $userDataHeader) {
                    $ud_len         = $this->hexToNum($this->substr($pduString, $start, 2));
                    $userDataHeader = '';
                    for ($i = 0; $i <= $ud_len; $i++) {
                        $userDataHeader .= $this->substr($pduString, $start + $i * 2, 2).' ';
                    }
                    if ($bitSize === ALPHABET_SIZE_7) {
                        $skipCharacters = ((($ud_len + 1) * 8) + 6) / 7;
                    } else {
                        $skipCharacters = ($ud_len + 1) / 2;
                    }
                }

                if ($bitSize === ALPHABET_SIZE_7) {
                    $userData = $this->getUserMessage($skipCharacters, $this->substr($pduString, $start, $this->stringLength($pduString) - $start), $messageLength);
                } else {
                    if ($bitSize === ALPHABET_SIZE_8) {
                        $userData = $this->getUserMessage8($skipCharacters, $this->substr($pduString, $start, $this->stringLength($pduString) - $start), $messageLength);
                        for ($i = 0; $i < $this->stringLength($userData); $i++) {
                            if ($this->substr($userData, $i, 1) >= ' ') {
                                $hex_dump[] = [
                                    'hex'     => $this->intToHex($this->charCodeAt($userData, $i)),
                                    'pointer' => $userData,
                                ];
                            } else {
                                $hex_dump[] = [
                                    'hex'     => $this->intToHex($this->charCodeAt($userData, $i)),
                                    'pointer' => '.',
                                ];
                            }
                        }
                    } else {
                        if ($bitSize === ALPHABET_SIZE_16) {
                            $userData = $this->getUserMessage16($skipCharacters, $this->substr($pduString, $start, $this->stringLength($pduString) - $start));
                        }
                    }
                }
                $userData = $this->substr($userData, 0, $messageLength);
                if ($bitSize === ALPHABET_SIZE_16) {
                    $messageLength /= 2;
                }
                $out['smsc']              = $SMSC_Number;
                $out['number']            = $sender_number;
                $out['toa']               = $senderTypeOfAddress.' '.$this->explainToa($senderTypeOfAddress);
                $out['timeStamp']         = $timeStamp;
                $out['timeStampTimeZone'] = $this->decodeTimezone($timezone);
                $out['tpPid']             = $tp_PID;
                $out['tpDcs']             = $tp_DCS;
                $out['tpDcsDesc']         = $tp_DCS_desc;
                if ($userDataHeader !== '') {
                    $out['userDataHeader'] = $userDataHeader;
                }
                $out['encoding'] = mb_detect_encoding($userData);
                $out['length']   = $messageLength;
                $out['message']  = $userData;
                if (! empty($hex_dump)) {
                    $out['hexadecimalDump'] = $hex_dump;
                }
            } else {
                $out[]            = 'SMS STATUS REPORT';
                $messageReference = $this->hexToNum($this->substr($pduString, $start, 2)); // ??? Correct this name
                $start            = $start + 2;
                // length in decimals
                $sender_addressLength = $this->hexToNum($this->substr($pduString, $start, 2));
                if ($sender_addressLength % 2 !== 0) {
                    $sender_addressLength += 1;
                }
                $start               = $start + 2;
                $senderTypeOfAddress = $this->substr($pduString, $start, 2);
                $start               = $start + 2;
                $sender_number       = $this->semiOctetToString($this->subString($pduString, $start, $start + $sender_addressLength));
                if (strtoupper($this->substr($sender_number, $this->stringLength($sender_number) - 1, 1)) === 'F') {
                    $sender_number = $this->subString($sender_number, 0, $this->stringLength($sender_number) - 1);
                }
                $start     += $sender_addressLength;
                $timezone  = $this->subString($this->semiOctetToString($this->substr($pduString, $start, 14)), 12, 14);
                $start     += 14;
                $timeStamp = $this->semiOctetToString($this->substr($pduString, $start, 14));
                // get date
                $year                              = $this->subString($timeStamp, 0, 2);
                $month                             = $this->subString($timeStamp, 2, 4);
                $day                               = $this->subString($timeStamp, 4, 6);
                $hours                             = $this->subString($timeStamp, 6, 8);
                $minutes                           = $this->subString($timeStamp, 8, 10);
                $seconds                           = $this->subString($timeStamp, 10, 12);
                $timezone2                         = $this->subString($timeStamp, 12, 14);
                $start                             += 14;
                $mStatus                           = $this->substr($pduString, $start, 2);
                $out['smsc']                       = $SMSC_Number;
                $out['number']                     = $sender_number;
                $out['toa']                        = $senderTypeOfAddress.' '.$this->explainToa($senderTypeOfAddress);
                $out['messageRef']                 = $messageReference;
                $out['timeStamp']                  = new \DateTime($year.'-'.$month.'-'.$day.' '.$hours.':'.$minutes.':'.$seconds.' GMT');
                $out['timeStampTimeZone']          = $this->decodeTimezone($timezone);
                $out['dischargeTimestamp']         = $timeStamp;
                $out['dischargeTimestampTimeZone'] = $this->decodeTimezone($timezone2);
                $out['status']                     = $mStatus.' '.$this->explainStatus($mStatus);
            }
        }

        $out['message'] = strip_tags($out['message']);

        return $out;
    }

    public function textToPdu($params) // AJA fixed SMSC processing
    {
        $params          = (object) $params;
        $message         = (isset($params->message)) ? ($params->message) : null;
        $receiver        = (isset($params->number)) ? $params->number : null;
        $smsc            = (isset($params->smsc)) ? $params->smsc : null;
        $alphabetSize    = (isset($params->alphabetSize)) ? $params->alphabetSize : ALPHABET_SIZE_7;
        $class           = (isset($params->class)) ? $params->class : null;
        $typeOfAddress   = (isset($params->typeOfAddress)) ? $params->typeOfAddress : null;
        $validity        = (isset($params->validity)) ? $params->validity : null;
        $receipt         = (isset($params->receipt)) ? $params->receipt : false;
        $octetSecond     = '';
        $output          = '';
        $smsInfoLength   = 0;
        $smsLength       = 0;
        $smsNumberFormat = '';
        $smscNumber      = '';
        if (! is_null($smsc)) {
            $smsNumberFormat = '81'; // national
            if ($this->substr($smsc, 0, 1) === '+') {
                $smsNumberFormat = '91'; // international
                $smsc            = $this->substr($smsc, 1);
            } else {
                if ($this->substr($smsc, 0, 1) !== '0') {
                    $smsNumberFormat = '91'; // international
                }
            }
            if ($this->stringLength($smsc) % 2 !== 0) {
                $smsc .= 'F';
            }
            $smscNumber    = $this->semiOctetToString($smsc);
            $smsInfoLength = ($this->stringLength(($smsNumberFormat.$smscNumber))) / 2;
            $smsLength     = $smsInfoLength;
        }
        if ($smsInfoLength < 10) {
            $smsInfoLength = '0'.$smsInfoLength;
        }
        if ($receipt) {
            if ($validity) {
                $firstOctet = '3100'; // 18 is mask for validity period // 10 indicates relative
            } else {
                $firstOctet = '2100';
            }
        } else {
            if ($validity) {
                $firstOctet = '1100';
            } else {
                $firstOctet = '0100';
            }
        }
        $receiverNumberFormat = '81'; // (national) 81 is "unknown"
        if ($this->substr($receiver, 0, 1) === '+') {
            $receiverNumberFormat = '91'; // international
            $receiver             = $this->substr($receiver, 1); //,$$phoneNumber.length-1);
        } else {
            if ($this->substr($receiver, 0, 1) !== '0') {
                $receiverNumberFormat = '91'; // international
            }
        }
        switch ($typeOfAddress) {
            case '145':
                $receiverNumberFormat = '91'; // international
                break;

            case '161':
                $receiverNumberFormat = 'A1'; // national
                break;

            case '129':
                $receiverNumberFormat = '81'; // unknown
                break;
        }
        $receiverNumberLength = $this->intToHex($this->stringLength($receiver));
        if ($this->stringLength($receiver) % 2 !== 0) {
            $receiver .= 'F';
        }
        $receiverNumber = $this->semiOctetToString($receiver);
        $protoId        = '00';
        $DCS            = 0;
        if ($class !== -1) // AJA
        {
            $DCS = $class | 0x10;
        }
        switch ($alphabetSize) {
            case ALPHABET_SIZE_7:
                break;
            case ALPHABET_SIZE_8:
                $DCS = $DCS | 4;
                break;
            case ALPHABET_SIZE_16:
                $DCS = $DCS | 8;
                break;
            default:
                return 'Invalid Alphabet Size';
        }
        $dataEncoding = $this->intToHex($DCS);
        $validPeriod  = ''; // AA
        if ($validity) {
            $validPeriod = $this->intToHex($validity); // AA
        }
        $userDataSize = null;
        if ($alphabetSize === ALPHABET_SIZE_7) {
            $tmp    = preg_split('//u', $message, -1, PREG_SPLIT_NO_EMPTY);
            $inpStr = '';
            for ($i = 0; $i < count($tmp); $i++) {
                if ($this->getSevenBitExt($tmp[$i])) {
                    $inpStr .= Utf8::unicodeToUtf8([0x1B]);
                }
                $inpStr .= $tmp[$i];
            }
            $inpStr       = preg_split('//u', $this->subString($inpStr, 0, $this->maxChars), -1, PREG_SPLIT_NO_EMPTY);
            $countInpStr  = count($inpStr);
            $userDataSize = $this->intToHex($countInpStr);
            for ($i = 0; $i <= $countInpStr; $i++) {

                if ($i === $countInpStr) {
                    if ($octetSecond !== '') {
                        $output .= ''.$this->intToHex($this->binToInt($octetSecond));
                    }
                    break;
                }
                if ($inpStr[$i] === Utf8::unicodeToUtf8([0x1B])) {
                    $current = $this->intToBin(0x1B, 7);
                } else {
                    $chr = $this->getSevenBitExt($inpStr[$i]);
                    if ($chr === 0) {
                        $chr = $this->getSevenBit($inpStr[$i]);
                    }
                    $current = $this->intToBin($chr, 7);
                }
                if ($i !== 0 && $i % 8 !== 0) {
                    $octetFirst   = $this->subString($current, 7 - ($i) % 8);
                    $currentOctet = $octetFirst.$octetSecond;    //put octet parts together
                    $output       .= ''.$this->intToHex($this->binToInt($currentOctet));
                    $octetSecond  = $this->subString($current, 0, 7 - ($i) % 8);    //set net second octet
                } else {
                    $octetSecond = $this->subString($current, 0, 7 - ($i) % 8);
                }
            }
        } else {
            if ($alphabetSize === ALPHABET_SIZE_8) {
                $userDataSize = $this->intToHex($this->stringLength($message));
                $explode      = preg_split('//u', $message, -1, PREG_SPLIT_NO_EMPTY);
                for ($i = 0; $i < count($explode); $i++) {
                    $CurrentByte = $this->charCodeAt($explode, $i);
                    $output      .= $this->toHex($CurrentByte);
                }
            } else {
                if ($alphabetSize === ALPHABET_SIZE_16) {
                    $userDataSize = $this->intToHex($this->stringLength($message) * 2);
                    $explode      = preg_split('//u', $message, -1, PREG_SPLIT_NO_EMPTY);
                    for ($i = 0; $i < count($explode); $i++) {
                        $myChar = $this->charCodeAt($explode, $i);
                        $output .= ''.$this->toHex(($myChar & 0xff00) >> 8).$this->toHex($myChar & 0xff);
                    }
                }
            }
        }
        $header = [
            $smsInfoLength,
            $smsNumberFormat,
            $smscNumber,
            $firstOctet,
            $receiverNumberLength,
            $receiverNumberFormat,
            $receiverNumber,
            $protoId,
            $dataEncoding,
            $validPeriod,
            $userDataSize,
        ];
        $pdu    = implode('', $header).$output;

        return [
            'byte_size' => ($this->stringLength($pdu) / 2 - $smsLength - 1),
            'message'   => $pdu,
        ];
    }

    public function filter($string)
    {
        return str_replace(['á', 'í', 'ó', 'ú', 'À', 'À', 'È', 'Ì', 'Í', 'Ò', 'Ó', 'Ù', 'Ú'], [
            'à',
            'ì',
            'ò',
            'ù',
            'A',
            'A',
            'E',
            'I',
            'I',
            'O',
            'O',
            'U',
            'U',
        ], $string);
    }

    public function stringLength($string, $encoding = 'UTF-8')
    {
        return mb_strlen($string, $encoding);
    }

    private function toHex($i)
    {
        $out = $this->hex[($i & 0xf)];
        $i   >>= 4;
        $out = $this->hex[($i & 0xf)].$out;

        return $out;
    }

    private function getSevenBit($character)
    {
        for ($i = 0; $i < count($this->sevenBitDefault); $i++) {
            if (Utf8::utf8ToUnicode($this->sevenBitDefault[$i]) === Utf8::utf8ToUnicode($character)) {
                return $i;
            }
        }

        return 0;
    }

    private function getSevenBitExt($character)
    {
        for ($i = 0; $i < count($this->sevenBitExtended); $i += 2) {
            if (Utf8::utf8ToUnicode($this->sevenBitExtended[$i]) === Utf8::utf8ToUnicode($character)) {
                return $this->sevenBitExtended[$i + 1];
            }
        }

        return 0;
    }

    public function getSevenBitExtendedAlphabet()
    {
        return $this->sevenBitExtended;
    }

    private function explainStatus($octet)
    {
        $octet_int = $this->parseInt($octet, 16);
        $status    = [
            0   => 'Ok,short message received by the SME',
            1   => 'Ok,short message forwarded by the SC to the SME but the SC is unable to confirm delivery',
            2   => 'Ok,short message replaced by the SC',
            32  => 'Still trying,congestion',
            33  => 'Still trying,SME busy',
            34  => 'Still trying,no response send SME',
            35  => 'Still trying,service rejected',
            36  => 'Still trying,quality of service not available',
            37  => 'Still trying,error in SME',
            64  => 'Error,remote procedure error',
            65  => 'Error,incompatible destination',
            66  => 'Error,connection rejected by SME',
            67  => 'Error,not obtainable',
            68  => 'Error,quality of service not available',
            69  => 'Error,no inter working available',
            70  => 'Error,SM validity period expired',
            71  => 'Error,SM deleted by originating SME',
            72  => 'Error,SM deleted by SC administration',
            73  => 'Error,SM does not exist',
            96  => 'Error,congestion',
            97  => 'Error,SME busy',
            98  => 'Error,no response sent SME',
            99  => 'Error,service rejected',
            100 => 'Error,quality of service not available',
            101 => 'Error,error in SME',
        ];

        return in_array($octet_int, $status) ? $status[$octet_int] : 'unknown';
    }

    private function decodeTimezone($timezone)
    {
        $tz     = $this->parseInt($this->subString($timezone, 0, 1), 16);
        $result = '+';

        if ($tz & 8) {
            $result = '-';
        }
        $tz = ($tz & 7) * 10;
        $tz += $this->parseInt($this->subString($timezone, 1, 2), 10);

        $tz_hour = floor($tz / 4);
        $tz_min  = 15 * ($tz % 4);

        if ($tz_hour < 10) {
            $result .= '0';
        }
        $result .= $tz_hour.':';
        if ($tz_min === 0) {
            $result .= '00';
        } else {
            $result .= $tz_min;
        }

        return $result;
    }

    private function explainToa($octet)
    {
        $result    = '';
        $p         = 'reserved';
        $octet_int = $this->parseInt($octet, 16);

        if ($octet_int !== -1) {
            switch (($octet_int & 0x70) >> 4) {
                case 0:
                    $p = 'unknown';
                    break;
                case 1:
                    $p = 'international';
                    break;
                case 2:
                    $p = 'national';
                    break;
                case 3:
                    $p = 'network specific';
                    break;
                case 4:
                    $p = 'subsciber';
                    break;
                case 5:
                    $p = 'alphanumeric';
                    break;
                case 6:
                    $p = 'abbreviated';
                    break;
            }

            $result .= $p;
            $p      = '';

            switch ($octet_int & 0x0F) {
                case 0:
                    $p = 'unknown';
                    break;
                case 1:
                    $p = 'ISDN/telephone';
                    break;
                case 3:
                    $p = 'data';
                    break;
                case 4:
                    $p = 'telex';
                    break;
                case 8:
                    $p = 'national';
                    break;
                case 9:
                    $p = 'private';
                    break;
                case 10:
                    $p = 'ERMES';
                    break;
            }

            if ($p !== '') {
                $p = 'Numbering Plan: '.$p;
            }
            $result .= ', '.$p;
        }

        return $result;
    }

    private function intToHex($i)
    {
        $h = '';
        $i = $this->parseInt($i);
        for ($j = 0; $j <= 3; $j++) {
            $h .= $this->hex[($i >> ($j * 8 + 4)) & 0x0F].$this->hex[($i >> ($j * 8)) & 0x0F];
        }

        return $this->subString($h, 0, 2);
    }

    private function cValid($valid)
    {
        $out   = '';
        $valid = $this->parseInt($valid);
        if ($valid <= 143) {
            $value = ($valid + 1) * 5; // Minutes
        } else {
            if ($valid <= 167) {
                $value = (($valid - 143) / 2 + 12); // Hours
                $value *= 60; // Convert to Minutes
            } else {
                if ($valid <= 196) {
                    $value = $valid - 166; // days
                    $value *= 60 * 24; // Convert to Minutes
                } else {
                    $value = $valid - 192; // Weeks
                    $value *= 7 * 60 * 24; // Convert to Minutes
                }
            }
        }
        $mins  = $value % 60;
        $hours = $value / 60;
        $days  = $hours / 24;
        $weeks = $days / 7;
        $hours %= 24;
        $days  %= 7;
        if ($this->parseInt($weeks) !== 0) {
            $out .= $this->parseInt($weeks).'w ';
        }
        if ($this->parseInt($days) !== 0) {
            $out .= $this->parseInt($days).'d ';
        }
        if ($this->parseInt($hours) !== 0) {
            $out .= $this->parseInt($hours).'h ';
        }
        if ($mins !== 0) {
            $out .= $mins.'m ';
        }

        return $out;
    }

    private function tpDCSMeaning($tp_DCS)
    {
        $tp_DCS_desc = $tp_DCS;
        $pomDCS      = $this->hexToNum($tp_DCS);
        $alphabet    = '';
        switch ($pomDCS & 192) {
            case 0:
                if ($pomDCS & 32) {
                    $tp_DCS_desc = 'Compressed Text';
                } else {
                    $tp_DCS_desc = 'Uncompressed Text';
                }
                if (! ($pomDCS & 16)) // AJA
                {
                    $tp_DCS_desc .= ', No class ';
                } else {
                    $tp_DCS_desc .= ', class: ';
                    switch ($pomDCS & 3) {
                        case 0:
                            $tp_DCS_desc .= '0 Flash ';
                            break;
                        case 1:
                            $tp_DCS_desc .= '1 ME specific ';
                            break;
                        case 2:
                            $tp_DCS_desc .= '2 SIM specific ';
                            break;
                        case 3:
                            $tp_DCS_desc .= '3 TE specific ';
                            break;
                    }
                }
                $tp_DCS_desc .= 'Alphabet: ';
                switch ($pomDCS & 12) {
                    case 0:
                        $tp_DCS_desc .= 'Default (7bit)';
                        break;
                    case 4:
                        $tp_DCS_desc .= '8bit';
                        break;
                    case 8:
                        $tp_DCS_desc .= 'UCS2 (16bit)';
                        break;
                    case 12:
                        $tp_DCS_desc .= 'Reserved';
                        break;
                }
                break;
            case 64:
            case 128:
                $tp_DCS_desc = 'Reserved coding group';
                break;
            case 192:
                switch ($pomDCS & 0x30) {
                    case 0:
                        $tp_DCS_desc = 'Message waiting group: Discard Message';
                        break;
                    case 0x10:
                        $tp_DCS_desc = 'Message waiting group: Store Message. Default Alphabet.';
                        break;
                    case 0x20:
                        $tp_DCS_desc = 'Message waiting group: Store Message. UCS2 Alphabet.';
                        break;
                    case 0x30:
                        $alphabet = 'Alphabet: ';
                        if (! ($pomDCS & 0x4)) {
                            $alphabet .= 'Default (7bit)';
                        } else {
                            $alphabet .= '8bit';
                        }
                        break;
                }
                if ($tp_DCS_desc === $tp_DCS) {
                    $tp_DCS_desc = 'Class: ';
                } else {
                    $tp_DCS_desc .= ', class: ';
                }

                switch ($pomDCS & 3) {
                    case 0:
                        $tp_DCS_desc .= '0 Flash';
                        break;
                    case 1:
                        $tp_DCS_desc .= '1 ME specific';
                        break;
                    case 2:
                        $tp_DCS_desc .= '2 SIM specific';
                        break;
                    case 3:
                        $tp_DCS_desc .= '3 TE specific';
                        break;
                }
                $tp_DCS_desc .= $alphabet;
                break;
        }

        return ($tp_DCS_desc);
    }

    private function phoneNumberMap($character)
    {
        if (($character >= '0') && ($character <= '9')) {
            return $character;
        }
        switch (strtoupper($character)) {
            case '*':
                return 'A';
            case '#':
                return 'B';
            case 'A':
                return 'C';
            case 'B':
                return 'D';
            case 'C':
                return 'E';
            default:
                return 'F';
        }
    }

    private function semiOctetToString($input)
    {
        $output = '';
        for ($i = 0; $i < $this->stringLength($input); $i = $i + 2) {
            $temp   = $this->subString($input, $i, $i + 2);
            $output .= $this->phoneNumberMap($temp[1]).$this->phoneNumberMap($temp[0]);
        }

        return $output;
    }

    private function hexToNum($numberS)
    {
        $tens = $this->makeNum($this->subString($numberS, 0, 1));
        $ones = 0;
        if ($this->stringLength($numberS) > 1) {
            $ones = $this->makeNum($this->subString($numberS, 1, 2));
        }
        if ($ones === 'X') {
            return '00';
        }

        return ($tens * 16) + ($ones * 1);
    }

    private function makeNum($string)
    {
        return CHAR_16BIT[strtoupper($string)];
    }

    private function charCodeAt($string, $index)
    {
        $charCodeAt = Utf8::utf8ToUnicode($string[$index]);

        return (isset($charCodeAt[0])) ? $charCodeAt[0] : false;
    }

    private function substr($str, $start, $length = null, $encoding = 'UTF-8')
    {
        if (isset($start) && isset($length)) {
            return mb_substr($str, $start, $length, $encoding);
        } else {
            if (isset($start) && ! isset($length)) {
                return mb_substr($str, $start);
            }
        }

        return false;
    }

    private function getUserMessage($skipCharacters, $input, $trueLength) // Add truelength AJA
    {
        $byteString   = '';
        $octetArray   = [];
        $restArray    = [];
        $septetsArray = [];
        $s            = 1;
        $count        = 0;
        $matchCount   = 0;
        $smsMessage   = '';
        $escaped      = 0;
        $charCounter  = 0;
        $byte_index   = 0;
        for ($i = 0; $i < $this->stringLength($input); $i = $i + 2) {
            $hex        = $this->subString($input, $i, $i + 2);
            $byteString .= $this->intToBin($this->hexToNum($hex), 8);
            $byte_index = $byte_index + 1;
        }
        for ($i = 0; $i < $this->stringLength($byteString); $i = $i + 8) {
            $octetArray[$count]   = $this->subString($byteString, $i, $i + 8);
            $restArray[$count]    = $this->subString($octetArray[$count], 0, ($s % 8));
            $septetsArray[$count] = $this->subString($octetArray[$count], ($s % 8), 8);
            $s++;
            $count++;
            if ($s === 8) {
                $s = 1;
            }
        }
        for ($i = 0; $i < count($restArray); $i++) {
            if ($i % 7 === 0) {
                if ($i !== 0) {
                    $charCounter++;
                    $charVal = $this->binToInt($restArray[$i - 1]);
                    if ($escaped) {
                        $smsMessage .= $this->getSevenBitExtendedCh($charVal);
                        $escaped    = 0;
                    } else {
                        if ($charVal === 27 && $charCounter > $skipCharacters) {
                            $escaped = 1;
                        } else {
                            if ($charCounter > $skipCharacters) {
                                $smsMessage .= $this->sevenBitDefault[$charVal];
                            }
                        }
                    }
                    $matchCount++; // AJA
                }
                $charCounter++;
                $charVal = $this->binToInt($septetsArray[$i]);
                if ($escaped) {
                    $smsMessage .= $this->getSevenBitExtendedCh($charVal);
                    $escaped    = 0;
                } else {
                    if ($charVal === 27 && $charCounter > $skipCharacters) {
                        $escaped = 1;
                    } else {
                        if ($charCounter > $skipCharacters) {
                            $smsMessage .= $this->sevenBitDefault[$charVal];
                        }
                    }
                }
                $matchCount++; // AJA
            } else {
                $charCounter++;
                $charVal = $this->binToInt($septetsArray[$i].$restArray[$i - 1]);
                if ($escaped) {
                    $smsMessage .= $this->getSevenBitExtendedCh($charVal);
                    $escaped    = 0;
                } elseif ($charVal === 27 && $charCounter > $skipCharacters) {
                    $escaped = 1;
                } elseif ($charCounter > $skipCharacters) {
                    $smsMessage .= $this->sevenBitDefault[$charVal];
                }
                $matchCount++; // AJA
            }
        }
        if ($matchCount !== $trueLength) // Don't forget trailing characters!! AJA
        {
            $charCounter++;
            $chval = $this->binToInt($restArray[$i - 1]);
            if (! $escaped) {
                if ($charCounter > $skipCharacters) {
                    $smsMessage .= $this->sevenBitDefault[$chval];
                }
            } else {
                if ($charCounter > $skipCharacters) {
                    $smsMessage .= $this->getSevenBitExtendedCh($chval);
                }
            }
        }

        return $smsMessage;
    }

    private function getUserMessage8($skipCharacters, $input, $truelength)
    {
        $smsMessage = '';
        // Cut the input string into pieces of 2 (just get the hex octets)
        for ($i = 0; $i < $this->stringLength($input); $i = $i + 2) {
            $hex        = $this->subString($input, $i, $i + 2);
            $smsMessage .= ''.(string) Utf8::unicodeToUtf8([$this->hexToNum($hex)]);
        }

        return $smsMessage;
    }

    private function getUserMessage16($skipCharacters, $input)
    {
        $smsMessage  = '';
        $charCounter = 0;
        // Cut the input string into pieces of 4
        for ($i = 0; $i < $this->stringLength($input); $i = $i + 4) {
            $hex1 = $this->subString($input, $i, $i + 2);
            $hex2 = $this->subString($input, $i + 2, $i + 4);
            $charCounter++;
            if ($charCounter > $skipCharacters) {
                $smsMessage .= ''.(string) Utf8::unicodeToUtf8([$this->hexToNum($hex1) * 256 + $this->hexToNum($hex2)]);
            }
        }

        return $smsMessage;
    }

    private function getSevenBitExtendedCh($character)
    {
        for ($i = 0; $i < count($this->sevenBitExtended); $i += 2) {
            if (Utf8::utf8ToUnicode($this->sevenBitExtended[$i + 1]) === Utf8::utf8ToUnicode($character)) {
                return $this->sevenBitExtended[$i];
            }
        }

        return '';
    }

    private function subString($string, $start = null, $end = null, $encoding = 'UTF-8')
    {
        if (isset($start) && isset($end)) {
            if ($start > $end) {
                [$start, $end] = [$end, $start];
            } else {
                if ($start < 0) {
                    $start = 0;
                }
            }

            return mb_substr($string, $start, $end - $start, $encoding);
        } else {
            if (isset($start) && ! isset($end)) {
                if ($start < 0) {
                    $start = 0;
                }

                return mb_substr($string, $start);
            }
        }

        return false;
    }

    private function parseInt($string, $base = 10)
    {
        return intval($string, $base);
    }

    private function intToBin($x, $size)
    {
        $num = $this->parseInt($x);
        $bin = decbin($num);
        for ($i = $this->stringLength($bin); $i < $size; $i++) {
            $bin = '0'.$bin;
        }

        return $bin;
    }

    private function binToInt($x)
    {
        $total = 0;
        $power = $this->parseInt($this->stringLength($x)) - 1;
        for ($i = 0; $i < $this->stringLength($x); $i++) {
            if ($x[$i] === '1') {
                $total = $total + pow(2, $power);
            }
            $power--;
        }

        return $total;
    }

    private function dcsBits($tpDCS)
    {
        $pomDCS = $this->hexToNum($tpDCS);
        switch (($pomDCS & 0x0C) >> 2) {
            case 1:
                return ALPHABET_SIZE_8;
            case 2:
                return ALPHABET_SIZE_16;
            default:
                return ALPHABET_SIZE_7;
        }
    }
}
